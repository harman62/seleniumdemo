import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

//Tests the input box page on SeleniumEasy.com
//URL = https://www.seleniumeasy.com/test/basic-first-form-demo.html
public class TestInputBoxes {
	
	WebDriver driver;

	//setup runs before the test case
	@Before
	public void setUp() throws Exception {
		//1. Enter some value into the text box
				//1. setup selenium + your webdriver
				//selenium + chrome
				System.setProperty("webdriver.chrome.driver","/Users/harman/Desktop/chromedriver");
				driver = new ChromeDriver();
				
				//2. go to the website
				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		System.out.println("This is setup");
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	//R1: test single input field
	@Test
	public void testSingleInputField() {
		
		
		//3. write code to do some stuff on that website
		
		//3a. type some nonsense into the single input box
		//get the box
		WebElement inputBox = driver.findElement(By.id("user-message"));
		//Type nonsense
		inputBox.sendKeys("here is some nonsense");
		//3b. Automatically push the submit button
		//Get the button
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		
		//2. Push the button
		showMessageButton.click();
		
		//3. Get the actual output from the screen
		WebElement outputBox = driver.findElement(By.id("display"));
		String actualOutput = outputBox.getText();
		
		//4. Check if expected output = actual output
		assertEquals("here is some nonsense", actualOutput);
		//fail("Not yet implemented");
	}
	
	//R2: test two input field
	@Test
	public void testTwoInputField() {
		
		WebElement inputBox1 = driver.findElement(By.id("sum1"));
		inputBox1.sendKeys("25");
		WebElement inputBox2 = driver.findElement(By.id("sum2"));
		inputBox2.sendKeys("50");
		WebElement button = driver.findElement(By.cssSelector("form#gettotal button"));
		button.click();
		WebElement outputBox = driver.findElement(By.id("displayvalue"));
		String actualOutput = outputBox.getText();
		//fail("Not yet implemented");
		assertEquals("75", actualOutput);
	}

}
