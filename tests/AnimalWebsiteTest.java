import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalWebsiteTest {
	//-------------------------
	//configuration variables
	//--------------------------
	
	//location of chromedriverfile
	final String CHROMEDRIVER_LOCATION = "/Users/harman/Desktop/chromedriver";
	
	//website we want to test
	final String URL_TO_TEST = "https://www.webdirectory.com/Animals/";
	
	//--------------------
	//global variables
	//--------------------
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		//1. Enter some value into the text box
		//1. setup selenium + your webdriver
		//selenium + chrome
		System.setProperty("webdriver.chrome.driver","/Users/harman/Desktop/chromedriver");
		driver = new ChromeDriver();
		
		//2. go to the website
		driver.get(URL_TO_TEST);
        System.out.println("This is setup");
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void testNoOfLinks() {
		// Just checking if number of links is equal to 10
				//1. Get all the bulleted llinks in section
				    List<WebElement> bulletedLinks = driver.findElements(By.cssSelector("table+ul li a"));
				    
				    
				    //----------------------------
//				    //Solution 2"
//				    
//				    //get all <ul> elements from the page
//				    List<WebElement> uls = driver.findElements(By.cssSelector("ul"));
//				    
//				    //get the first ul
//				    WebElement firstUl = uls.get(0);
//				    
//				    //get all the links inside the first <ul>
//				    List<WebElement> bulletLinks = firstUl.findElements(By.cssSelector("li a"));
//				    
//				    //-----------------------------
				    
				    System.out.println("Number of links on page: "+bulletedLinks.size());
				    
				 //2. Output the links to the screen using System.out.println()
				     //Iterating through the list of links
				    for(int i=0; i < bulletedLinks.size(); i++)
				    {
				    	//Get the current link
				    	  WebElement link = bulletedLinks.get(i);
				    	  
				    	//get the link Text
				    	  String linkText = link.getText();
				    	  
				    	 //Get link url
				    	  String linkURL = link.getAttribute("href");
				    	  
				    	  //OR   String linkUrl = link.getAttribute("class")
				    	  //     
				    	  
				    	  
				    	  
				    	 //Output to screen
				    	  System.out.println(linkText + ": " +linkURL);
				    	  
				    }
	}

}
