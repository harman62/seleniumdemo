import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {

	public static void main(String[] args) throws InterruptedException {
	//1. setup selenium + your webdriver
	//selenium + chrome
	System.setProperty("webdriver.chrome.driver","/Users/harman/Desktop/chromedriver");
	WebDriver driver = new ChromeDriver();
	
	//2. go to the website
	driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
	
	//3. write code to do some stuff on that website
	
	//3a. type some nonsense into the single input box
	//get the box
	WebElement inputBox = driver.findElement(By.id("user-message"));
	//Type nonsense
	inputBox.sendKeys("here is some nonsense");
	//3b. Automatically push the submit button
	//Get the button
	WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
	//push the button
	showMessageButton.click();
	
	
	//4. close the driver
	Thread.sleep(5000);
	driver.close();
}
}
